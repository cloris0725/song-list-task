import React from 'react';
import './App.css';
import Player from "./component/player/player";
import PlayerList from "./component/playList/playList";

function App() {
  return (
    <div className="player">
      <Player></Player>
        <PlayerList></PlayerList>
    </div>
  );
}

export default App;
