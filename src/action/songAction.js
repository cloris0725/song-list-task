//state, store, reducer


//state: stands for data to the app extent   当前应用数据
//store: the place saving state              内存，定义了规则，存放整个state
//reducer:the place to update state, the definition of how to change state
//action是一个函数 箭头函数 arrow function  是用来 14:00   可以默认为是产生数据的地方  **数据--- state   从网络端抓数据
//reducer是把action产生的数据（改变的数据）存放、更新到store里面，一个action对应一个reducer


// component, reducer, action
//reducer and action 都是用来给component服务的
//通过connect传递到component里面
//所有的state、reducer、action都是全局的，都可以随意使用

//action返回一个object or function
//function -> redux-thunk, middleware
//reducer一定要有一个返回值（return），return的是存放最新的state

import {FETCH_ALL_SONGS} from "../helper"


const fetchAllSongs = () => {
    return{
        type: FETCH_ALL_SONGS,
        payload: [
            {id:1, name: 'One Kiss', artist:"Taylor", length:3.34},
            {id:2, name: 'One Kiss2', artist:"Taylor", length:3.34},
            {id:3, name: 'One Kiss3', artist:"Taylor", length:3.34},
            {id:4, name: 'One Kiss4', artist:"Taylor", length:3.34}

            ]
    }

}

export default fetchAllSongs

