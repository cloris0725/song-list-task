import React, {Component} from "react";
import "./playList.css"

class SongItem extends Component {
    constructor() {
        super();
        this.state = {hoveredSongId: -1, selectedSong: [], hideAllCheck: false}
    }
    render() {
        return this.props.songs.map(song => {
            let hovered = this.state.hoveredSongId === song.id
            let classHighlight = 'songItem'
            if (hovered){
                classHighlight = 'highLight'
            }

            const alternativeView = hv => {return {visibility: hv? 'hidden' : 'visible' }}
            const alternativeDisplay = hv => {return {display: hv? 'flex': 'none'}}


            return (
                <div className={classHighlight} key={song.id}
                         onMouseEnter={_ => this.setState({hoveredSongId: song.id})}
                onMouseLeave={_ => this.setState({hoveredSongId: -1})}>

                <div className="chkBox"
                style={alternativeView(!hovered || this.state.hideAllCheck)}>
                    {/*<i className="far fa-check-square"></i>*/}
                    <i className="far fa-square" onClick={_ => this.setState({hideAllCheck: true})}></i></div>
                <div className="chkBox"
                     style={alternativeDisplay(!hovered)}>
                    {song.id}
                </div>
                    <div className="chkBox"
                         style={alternativeDisplay(hovered)}>

                        <i className="far fa-play-circle"></i>
                    </div>
                <div className="chkBox"><img src="https://album-art-storage-us.s3.amazonaws.com/703cd1ecd4566057919
                129566d38834488cce570ad9d1e7ab64eaa7058a90fd3_110x110.jpg" width="40" alt=""/></div>
                <div><div>{song.name}</div><div>{song.artist}</div></div>
                <div className="lengthBox">
                    {song.length}
                    <i className="fas fa-ellipsis"></i>
                </div>
            </div>
            )})
}}

export default SongItem


