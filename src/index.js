import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Provider} from "react-redux"
import {createStore} from "redux";
import SongReducer from "./reducer/songReducer"



ReactDOM.render(
    <Provider store={createStore(SongReducer)}>
<App />
</Provider>
,
  document.getElementById('root')
);

//原生react和redux都是单独的系统,想要建立联系就要 import {Provider} from react-redux, reducer产生的新数据会存在store里面；
//用store来连接，Provider有一个property是store，同时用reducer的一个接口--->{createStore}导入 （import {createStore} from “redux”） 这样原生react就可以和redux建立链接
//这样子框架就构建好，可以开始使用了，去component里面映射reducer
