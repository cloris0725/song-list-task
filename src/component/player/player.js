import React, {Component} from "react";
import './player.css'
import PlayerConsole from "./playerConsole";
import Cover from "./cover";
class Player extends Component {
    render() {
        return (<div className="topPlayer">Player
            <Cover></Cover>
            <PlayerConsole></PlayerConsole>
        </div>)
    }
}

export default Player
