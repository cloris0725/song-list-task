
import {FETCH_ALL_SONGS} from "../helper"
import {combineReducers} from "redux";

const initState = []

//reducer首先需要一个初始值, 因为component一渲染就要消费state，但不一定能拿到值（有可能要等从服务器抓数据）
// reducer一定要有一个返回值（return）, return的是存放最新的state, 返回的是object,array or other things,根据action里面的东西判断
//three dot operator/ spread operator, es2015 new features 三点运算符
//产生一个新的内存地址  数据34:55 copy原有的array，object数据
//immutability, array, object -->
//不能在reducer里面改变state，但是可以生成新的state，然后改变里面的内容,所以要用...state, 把原来的数据copy下来，再把新的array的数据传输进来...action.payload

const songReducer = (state= initState, action) => {
    switch (action.type){
        case FETCH_ALL_SONGS:
            console.log('fetch all songs...>>>>', action.payload)
            // return [...action.payload] or return [...state, ...action.payload]
            return action.payload

        default:
            console.log('fetch all songs...>>>>', action.payload)
            return state
    }
}

export default combineReducers({
    songReducer
//    也有可能有无数个reducers
})

//reducer和component想要关联，必须要利用一定的机制，那就是redux.通过redux接口被连接起来，combineReducer把所有的reducer合并起来
//reducer想要被使用，必须引入combineReducers（在开头import combineReducers from ‘react-redux’）, 然后我们定义的每一个reducer，就被写成一个个object，例如 line 27， 就被集合出去
//在component里面想要建立桥梁 那就在最后一行写 export default connect（）（）--->这个connect函数返回的是closure：两层函数  第一次返回的函数是closure，然后再次调用函数（第二个括号里面写component的名字）

// 哪些 Redux 全局的 state 是我们组件想要通过 props 获取的？
// mapStateToProps ——> reducers

// 哪些 action 创建函数是我们想要通过 props 获取的？
// mapDispatchToProps ——> actions


//[mapStateToProps(state, [ownProps]): stateProps] (Function): 如果定义该参数，组件将会监听 Redux store 的变化。任何时候，只要 Redux store 发生改变，mapStateToProps 函数就会被调用。
//






