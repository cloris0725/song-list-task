import React, {Component} from "react";
import "./playList.css"
import SongItem from "./songItem"
import {connect} from "react-redux";


let countRender = 0
class PlayerList extends Component {
    render() {
        console.log(`render ${countRender++}`)
        return (<div className="playList">
              <SongItem songs={this.props.songs}></SongItem>
            </div>)
    }
}

//closure
// 这个回调函数有几个参数，最常见的是两个---state，ownPros，至少要包含state
// return是一个object
//
const mapStateToPros = (state) => {
    return {
        songs: state.songReducer
    }
}

export default connect(mapStateToPros)(PlayerList)
//在component里面想要建立桥梁 那就在最后一行写 export default connect（）（）--->这个connect函数返回的是closure：两层函数  第一次返回的函数是closure，然后再次调用函数（第二个括号里面写component的名字）
//想要让reducers和component关联-- 第一层函数返回一个 closure (第一个括号) ---> 可以传两个参数：第一个参数可以是好多个reducers，第二个参数是好多个actions. 但这么写会要写很多，所以我们就用mapStateToPros代替

// mapStateToPros() return出来的值--object，最后都会映射成component的一个props--属性  class-based的默认属性--props



